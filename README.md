# Payroll app

This project is a task for a [Robusta Studio](https://robustastudio.com/) interview.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

- A PHP development stack is required for this project to run
- Linux machine.
- Composer.


### Installing

1. Clone the repo ``$ git clone https://gitlab.com/osama-interviews-tasks/payroll-app.git``
2. `cd` into the project folder and Install the composer packages ``$ cd payroll-app && composer install``
3. Set you environments variables inside a ``.env`` file in the project root folder
4. Run the database migrations ``$ php artisan migrate``
5. Run the seeders ``$ php artisan db:seed``

After finishing all the steps above, you should have a database that has 51 users, with the first user as an admin.


## Usage
The application consists of only two endpoints, as follows:
 1. Get Year Payment Data:
     Request: 

    ```
    GET /api/get_year_data?api_token={api_token} HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    ```
    Response:
    ```
    [
        {
            "month": "Jan",
            "salaries_payment_day": 31,
            "bonus_payment_day": 15,
            "salaries_total": 288805,
            "bonus_total": 28880.5
            "payments_total": 317685.5
        }...
    ]
    ```
 2. Change employees bonus percentage:
     Request:
    ```
    POST /api/employee/{user_id}/change_bonus?api_token={api_token} HTTP/1.1
    Accept: application/json
    Content-Type: application/json

    {
        "new_bonus_percentage": 25
    }
    ```
    Response:
    ```
    {
        "message": "Success"
    }
    ```

## Built With

* [Laravel](https://laravel.com/) - The web framework used
* [Laravel Permissions](https://github.com/spatie/laravel-permission) - The package used for roles and permissions
* [Carbon](https://carbon.nesbot.com/) - The API extension used for DateTime

## Authors

* **Osama Aldemeery**  - [Github](https://github.com/aldemeery) | [Gitlab](https://gitlab.com/aldemeery)
