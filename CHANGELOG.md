# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2018-10-16
### Added
 - Database migrations.
 - Database seeders.
 - Payment reminder command.
 - 'Get year payments' endpoint
 - 'Change employee bonus percentage' endpoint.
