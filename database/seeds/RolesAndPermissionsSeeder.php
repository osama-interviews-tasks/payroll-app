<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permission
        Permission::create(['name' => 'access api']);

        // create role and assign created permission
        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('access api');
    }
}
