<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create the admin
        factory(App\User::class)->create()->assignRole('admin');

        // create other employees
        factory(App\User::class, 50)->create();
    }
}
