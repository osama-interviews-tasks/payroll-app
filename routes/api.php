<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['auth:api', 'permission:access api']], function() {
    Route::get("get_year_data", "PayrollsController@getYearData");
    Route::post("employee/{user}/change_bonus", "UsersController@changeBonus");
});

