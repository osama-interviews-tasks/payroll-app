<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class CarbonMacrosServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::macro('getSalaryDay', function(){
            $date = clone $this;
            $date->endOfMonth();
            if ($date->isFriday() || $date->isSaturday()) {
                $date->previous(Carbon::THURSDAY);
            }

            return $date;
        });

        Carbon::macro('getBonusDay', function(){
            $date = clone $this;
            $date->day(15);
            if ($date->isFriday() || $date->isSaturday()) {
                $date->next(Carbon::THURSDAY);
            }

            return $date;
        });

        Carbon::macro('isSalaryDay', function(){
            return $this->isSameDay($this->getSalaryDay());
        });

        Carbon::macro('isBonusDay', function(){
            return $this->isSameDay($this->getBonusDay());
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
