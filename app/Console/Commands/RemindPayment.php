<?php

namespace App\Console\Commands;

use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class RemindPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:remind';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind admins with payment days';

    protected $date;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Carbon $date)
    {
        $this->date = $date;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admins = User::role('admin')->get();
        $users = User::all();

        $this->date->addDays(2);

        if ($this->date->isSalaryDay()) {
            $salaries = $users->sum('base_salary');
            foreach ($admins as $admin) {
                Mail::send('emails.salary', compact('salaries'), function ($message) use ($admin) {
                    $message->to($admin->email);
                });
            }
        }

        if ($this->date->isBonusDay()) {
            $bonuses = $users->sum('bonus_percentage');
            foreach ($admins as $admin) {
                Mail::send('emails.bonus', compact('bonuses'), function ($message) use ($admin) {
                    $message->to($admin->email);
                });
            }
        }
    }
}
