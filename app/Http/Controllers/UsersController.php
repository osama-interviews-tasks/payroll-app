<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangeEmployeeBonusRequest;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function changeBonus(User $user, ChangeEmployeeBonusRequest $request)
    {
        $user->bonus_percentage = $request->new_bonus_percentage;
        $user->save();

        return response()->json(['message' => 'Success']);
    }
}
