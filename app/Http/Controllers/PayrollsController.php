<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PayrollsController extends Controller
{
    public function getYearData()
    {
        $data = [];

        $employees = User::all();
        $salaries = $employees->sum('base_salary');
        $bonuses = $employees->sum(function($employee) {
            return $employee->base_salary * $employee->bonus_percentage / 100;
        });
        $payment = $employees->sum('total_salary');

        $date = Carbon::parse(date('Y'));

        for ($month = 1; $month <= 12; $month++) {
            $date->month($month);

            $temp["month"] = $date->shortEnglishMonth;
            $temp["salaries_payment_day"] = $date->getSalaryDay()->day;
            $temp["bonus_payment_day"] = $date->getBonusDay()->day;
            $temp["salaries_total"] = $salaries;
            $temp["bonus_total"] = $bonuses;
            $temp["payments_total"] = $payment;

            $data[] = $temp;
        }

        return response()->json($data);
    }
}
